const allowedOrigins = (
    'http://localhost:3001',
    'https://www.prototype.com',
    'https://www.google.com'
)

module.exports = allowedOrigins
